SHELL:=/bin/bash
.DEFAULT_GOAL := build

#######
# hard to remember golang vendoring commands
#######
.PHONY: dependencies
dependencies:
	# Update all direct and indirect dependencies to latest minor or patch upgrades
	go get -u ./...
	# Prune any no-longer-needed dependencies from go.mod and add any dependencies needed for other combinations of OS, architecture, and build tags
	go mod tidy

#######
# building, testing
#######
.PHONY: build
build: webui
	mkdir -p build
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o build/cactus-gitops -ldflags="-w -s" -tags=production ./cmd

.PHONY: test
test:
	mkdir -p build/coverage
	go test ./... -v -covermode=count -coverprofile=build/coverage/report.out 2>&1 | tee report.out
	go tool cover -html=build/coverage/report.out -o build/coverage/index.html

.PHONY: clean
clean:
	rm -rf build ${webui_folder}/production.go

#######
# web interface
#######
webui_folder := web/ui
webui_src_files := $(shell find ${webui_folder} -type f | grep -v -e 'node_modules' -e 'production.go' | sed 's|\s|\\ |g')

${webui_folder}/node_modules:
	# --unsafe-perm to make it work in docker containers
	cd ${webui_folder} && npm i --unsafe-perm

${webui_folder}/build: ${webui_folder}/node_modules $(webui_src_files)
	cd ${webui_folder} && npx vue-cli-service lint --max-warnings 0 && npx vue-cli-service build --mode production

${webui_folder}/production.go: ${webui_folder}/build
	go generate ./web/ui/generator/production.go

.PHONY: webui
webui: ${webui_folder}/production.go