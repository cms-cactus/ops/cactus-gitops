package sync

import (
	"cactus-gitops/internal/config"
	"cactus-gitops/internal/gitlab"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

// Config holds the content of the .cactus-gitops.yml
// config file of a project
type Config struct {
	Project gitlab.Project
}

// NoSyncConfigError gets returned when a project config gets
// asked for a project that doesn't have one
type NoSyncConfigError struct {
	Project config.ProjectID
}

func (e NoSyncConfigError) Error() string {
	return fmt.Sprintf("project %d is not configured for cactus gitops", e.Project)
}

// GetProjectConfig reads the configuration file
// from the default branch of the given project
// if the project does not contain a config file
// it is considered not wanting to participate
// and skipped
func GetProjectConfig(project gitlab.Project) (*Config, error) {
	_id := fmt.Sprintf("%d", project.ID)
	URL := "/projects/" + _id + "/repository/files/.cactus-gitops.yml/raw?ref=" + project.DefaultBranch

	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("PRIVATE-TOKEN", os.Getenv("GITLAB_TOKEN"))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 404 {
		return nil, NoSyncConfigError{project.ID}
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("http request failed (get gitops config in project " + _id + " returned " + resp.Status + ")")
	}

	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	syncConfig := Config{}
	err = json.Unmarshal(bytes, &syncConfig)
	if err != nil {
		return nil, err
	}
	syncConfig.Project = project

	return &syncConfig, nil
}
