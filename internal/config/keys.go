package config

// list of config keys to be used with viper.Get(<name>)
var (
	ConfigLoglevel                    = newStringKey("log.level", "log level", "info")
	ConfigAPIPort                     = newIntKey("api.port", "API port to listen to", 80)
	ConfigGitlabProjects              = newStringArrayKey("gitlab.projects", "list of GitLab projects to sync", []string{})
	ConfigGitlabGroups                = newStringArrayKey("gitlab.groups", "list of GitLab groups to sync", []string{"10629"})
	ConfigGitlabRefreshProjectList    = newStringKey("gitlab.refresh.project_list", "project list refresh rate", "1h")
	ConfigGitlabRefreshProjectContent = newStringKey("gitlab.refresh.project_content", "project content refresh rate", "1m")
)

var allPaths = []string{
	ConfigLoglevel.Path,
	ConfigAPIPort.Path,
	ConfigGitlabProjects.Path,
	ConfigGitlabGroups.Path,
	ConfigGitlabRefreshProjectList.Path,
	ConfigGitlabRefreshProjectContent.Path,
}
