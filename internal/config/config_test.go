package config

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

func readMemConfig(t *testing.T, c string) error {
	file, err := ioutil.TempFile("", "*.yml")
	require.Nil(t, err)
	defer os.Remove(file.Name())
	file.WriteString(c)
	return ReadConfig(file.Name())
}

func checkAllConfigs(t *testing.T, logLevel string, gitlabProjects []string, gitlabGroups []string, apiPort int) {
	require.Equal(t, logLevel, ConfigLoglevel.Get())
	require.ElementsMatch(t, gitlabProjects, ConfigGitlabProjects.Get())
	require.ElementsMatch(t, gitlabGroups, ConfigGitlabGroups.Get())
	require.Equal(t, apiPort, ConfigAPIPort.Get())
}

func TestConfigReadsAllFields(t *testing.T) {
	err := readMemConfig(t, `
log:
  level: trace
gitlab:
  projects:
  - project1
  - project2
  groups:
    - group1
    - group2
api:
  port: 1
  `)
	require.Nil(t, err)
	checkAllConfigs(t, "trace", []string{"project1", "project2"}, []string{"group1", "group2"}, 1)
}

func TestMinimalConfigWorks(t *testing.T) {
	err := readMemConfig(t, `
log:
  level: trace`)
	require.Nil(t, err)
	require.Equal(t, "trace", ConfigLoglevel.Get())
}

func TestConfigKeySpellingError(t *testing.T) {
	err := readMemConfig(t, `
log:
  levell: trace`)
	require.NotNil(t, err)
	require.Equal(t, "unknown configuration key: log.levell", err.Error())
}

func TestConfigSetsAllDefaults(t *testing.T) {
	err := readMemConfig(t, "")
	require.Nil(t, err)
	checkAllConfigs(t, "info", []string{}, []string{"10629"}, 80)
}

func TestConfigReloadWorks(t *testing.T) {
	err := readMemConfig(t, `
  log:
    level: trace
  gitlab:
    projects:
    - project1
    - project2
    groups:
      - group1
      - group2
  api:
    port: 1
    `)
	require.Nil(t, err)
	checkAllConfigs(t, "trace", []string{"project1", "project2"}, []string{"group1", "group2"}, 1)
	err = readMemConfig(t, `
  log:
    level: warn
  gitlab:
    projects:
    - project3
    - project4
    groups:
      - group3
      - group4
  api:
    port: 2
    `)
	require.Nil(t, err)
	checkAllConfigs(t, "warn", []string{"project3", "project4"}, []string{"group3", "group4"}, 2)
}

func TestCLIOverridesWork(t *testing.T) {
	// unfortunately, flags are only parsed once, so setting os.Args is not going to work
	// instead, we can only manually call Set on flags and trust that the flag package
	// does that for us in production
	// os.Args = []string{os.Args[0], ...}
	ConfigLoglevel.flag.Set("warn")
	ConfigAPIPort.flag.Set("3")
	// array arguments are populated by specifying the argument multiple times
	// e.g. progname --gitlab.projects a --gitlab.projects b
	ConfigGitlabProjects.flag.Set("project3")
	ConfigGitlabProjects.flag.Set("project4")
	ConfigGitlabGroups.flag.Set("group3")
	ConfigGitlabGroups.flag.Set("group4")
	err := readMemConfig(t, `
  log:
    level: trace
  gitlab:
    projects:
    - project1
    - project2
    groups:
      - group1
      - group2
  api:
    port: 1
    `)
	require.Nil(t, err)
	checkAllConfigs(t, "warn", []string{"project3", "project4"}, []string{"group3", "group4"}, 3)
	ConfigLoglevel.flag.IsSet = false
	ConfigAPIPort.flag.IsSet = false
	ConfigGitlabProjects.flag.IsSet = false
	ConfigGitlabProjects.flag.IsSet = false
	ConfigGitlabGroups.flag.IsSet = false
	ConfigGitlabGroups.flag.IsSet = false
}

func TestEnvFallbackWorks(t *testing.T) {
	os.Setenv("GITLAB_SYNC_LOG_LEVEL", "error")
	defer os.Unsetenv("GITLAB_SYNC_LOG_LEVEL")
	err := readMemConfig(t, "")
	require.Nil(t, err)
	require.Equal(t, "error", ConfigLoglevel.Get())
}

func TestShittyLogLevel(t *testing.T) {
	err := readMemConfig(t, `
  log:
    level: shitty
`)
	require.NotNil(t, err)
}

func TestValidLogLevels(t *testing.T) {
	levels := map[string]logrus.Level{
		"trace":   logrus.TraceLevel,
		"debug":   logrus.DebugLevel,
		"info":    logrus.InfoLevel,
		"warn":    logrus.WarnLevel,
		"warning": logrus.WarnLevel,
		"error":   logrus.ErrorLevel,
		"fatal":   logrus.FatalLevel,
	}
	for key, level := range levels {
		err := readMemConfig(t, `
    log:
      level: `+key+`
  `)
		require.Nil(t, err)
		l := logrus.GetLevel()
		require.Equal(t, level, l)
	}
}

func TestCorruptConfig(t *testing.T) {
	err := readMemConfig(t, `kk`)
	require.NotNil(t, err)
}

func TestNonExistingConfig(t *testing.T) {
	// no config found
	os.Setenv("XDG_CONFIG_HOME", "/tmp")
	os.Setenv("HOME", "/tmp")
	err := ReadConfig("")
	// require.Contains(t, err.Error(), "no such file or directory")
	require.NotNil(t, err)
	_, ok := err.(viper.ConfigFileNotFoundError)
	require.True(t, ok)

	// config specified does not exist
	file, err := ioutil.TempFile("", "*.yml")
	require.Nil(t, err)
	os.Remove(file.Name())
	err = ReadConfig(file.Name())
	require.NotNil(t, err)
	require.Contains(t, err.Error(), "no such file or directory")
	_, ok = err.(viper.ConfigFileNotFoundError)
	require.False(t, ok)
}
