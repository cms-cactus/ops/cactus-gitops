package config

import (
	configflags "cactus-gitops/internal/config/flags"
	"flag"

	"github.com/spf13/viper"
)

// IntKey is an integer type configuration key
type IntKey struct {
	*KeyDefinition
	flag configflags.IntFlag
}

// Get gets it
func (k *IntKey) Get() int {
	if k.flag.IsSet {
		return k.flag.Value
	}
	return viper.GetInt(k.Path)
}

func newIntKey(path string, description string, defaultValue int) *IntKey {
	k := newKeyDefinition(path, description, defaultValue)
	v := configflags.IntFlag{}
	flag.Var(&v, path, description)
	return &IntKey{k, v}
}
