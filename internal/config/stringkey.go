package config

import (
	configflags "cactus-gitops/internal/config/flags"
	"flag"

	"github.com/spf13/viper"
)

// StringKey is a string type configuration key
type StringKey struct {
	*KeyDefinition
	flag configflags.StringFlag
}

// Get gets it
func (k *StringKey) Get() string {
	if k.flag.IsSet {
		return k.flag.Value
	}
	return viper.GetString(k.Path)
}

func newStringKey(path string, description string, defaultValue string) *StringKey {
	k := newKeyDefinition(path, description, defaultValue)
	v := configflags.StringFlag{}
	flag.Var(&v, path, description)
	return &StringKey{k, v}
}
