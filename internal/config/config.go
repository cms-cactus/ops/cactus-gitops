package config

import (
	"errors"
	"flag"
	"os"
	"os/signal"
	"path"
	"strings"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// GroupID uniquely identifies a group in GitLab
type GroupID int

// ProjectID uniquely identifies a project in GitLab
type ProjectID int

const envPrefix = "GITLAB_SYNC"

// config a.b becomes GITLAB_SYNC_A_B
var envTransformer = strings.NewReplacer(".", "_")

// ReadConfig sets up viper and searches for a config file
// (re-)reads the config file it finds, or returns an error
func ReadConfig(location string) error {
	// set search paths, or use override config file
	if location != "" {
		log.WithField("file", location).Info("config file location overridden")
		viper.SetConfigFile(location)
	} else {
		// search in current working directory
		viper.AddConfigPath(".")
		// search in standard config folder, if set
		if os.Getenv("XDG_CONFIG_HOME") != "" {
			viper.AddConfigPath(os.Getenv("XDG_CONFIG_HOME"))
		}
		// search in home folder, if set
		if os.Getenv("HOME") != "" {
			viper.AddConfigPath(path.Join(os.Getenv("HOME"), ".config/cactus-gitops/"))
		}
		// read https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf
		// page 9 specifies why to prefer /etc/opt
		viper.AddConfigPath("/etc/opt/cactus/cactus-gitops")
		viper.AddConfigPath("/opt/cactus/cactus-gitops")

		// search for cactus-gitops.(json|toml|yaml|yml|properties|props|prop|env|dotenv)
		viper.SetConfigName("cactus-gitops")
	}

	// read in env variables, env variables override config file
	// example: 'loglevel: info' in config.yaml will be overriden by command
	//          'GITLAB_SYNC_LOGLEVEL=ERROR go run main.go'
	viper.SetEnvPrefix(envPrefix)
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(envTransformer)

	// allows for smarter deserialization of values
	viper.SetTypeByDefaultValue(true)

	// find and read config, return error on failure
	err := viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.WithError(err).WithField("file", location).Error("specified config file not found")
			return err
		} else {
			log.WithError(err).WithField("file", viper.ConfigFileUsed()).Error("an error occurred while reading config file")
			return err
		}
	}
	log.WithField("file", viper.ConfigFileUsed()).Info("config file found")

	// spell checking
	for _, key := range viper.AllKeys() {
		match := false
		for _, known := range allPaths {
			if key == known {
				match = true
			}
		}
		if !match {
			return errors.New("unknown configuration key: " + key)
		}
	}

	// read in CLI arguments
	// it is safe to call flag.Parse() yourself earlier than ReadConfig()
	flag.Parse()

	// straight away, adjust logrus log level
	// loglevel := viper.GetString(KeyLoglevel.Path)
	loglevel := ConfigLoglevel.Get()
	if loglevel != "" {
		log.WithField("level", loglevel).Info("setting log level")
	}

	var level logrus.Level = logrus.InfoLevel
	switch loglevel {
	case "trace":
		level = logrus.TraceLevel
	case "debug":
		level = logrus.DebugLevel
	case "info":
		level = logrus.InfoLevel
	case "warn", "warning":
		level = logrus.WarnLevel
	case "error":
		level = logrus.ErrorLevel
	case "fatal":
		level = logrus.FatalLevel
	default:
		log.WithField("level", loglevel).Error("log level specified in config does not exist")
		return errors.New("requested log level does not exist: " + loglevel)
	}
	logrus.SetLevel(level)

	return nil
}

// ReadConfigAndReloadOnSighup runs ReadConfig()
// when called as well as whenever the HUP signal is received
func ReadConfigAndReloadOnSighup(location string) error {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP)

	err := ReadConfig(location)
	if err != nil {
		return err
	}
	go func() {
		for sig := range c {
			log.WithField("sig", sig).Info("received SIGHUP. Reloading config")
			err := ReadConfig(location)
			if err != nil {
				log.WithError(err).Error("new config not loaded")
			}
		}
	}()
	return nil
}
