package configflags

import (
	"fmt"
	"strconv"
)

type IntFlag struct {
	Value int
	IsSet bool
}

func (sf *IntFlag) Set(x string) error {
	v, err := strconv.Atoi(x)
	if err != nil {
		return err
	}
	sf.Value = v
	sf.IsSet = true
	return nil
}

func (sf *IntFlag) String() string {
	return fmt.Sprint(sf.Value)
}
