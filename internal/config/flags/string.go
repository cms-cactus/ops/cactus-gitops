package configflags

import "fmt"

type StringFlag struct {
	Value string
	IsSet bool
}

func (sf *StringFlag) Set(x string) error {
	fmt.Println("setting", x)
	sf.Value = x
	sf.IsSet = true
	return nil
}

func (sf *StringFlag) String() string {
	return sf.Value
}
