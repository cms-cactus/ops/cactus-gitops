package configflags

import "strings"

type StringArrayFlag struct {
	Value []string
	IsSet bool
}

func (sf *StringArrayFlag) Set(x string) error {
	sf.Value = append(sf.Value, x)
	sf.IsSet = true
	return nil
}

func (sf *StringArrayFlag) String() string {
	return strings.Join(sf.Value, " ")
}
