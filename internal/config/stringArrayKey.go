package config

import (
	configflags "cactus-gitops/internal/config/flags"
	"flag"

	"github.com/spf13/viper"
)

// StringArrayKey is a string array type configuration key
type StringArrayKey struct {
	*KeyDefinition
	flag configflags.StringArrayFlag
}

// Get gets it
func (k *StringArrayKey) Get() []string {
	if k.flag.Value != nil {
		return k.flag.Value
	}
	return viper.GetStringSlice(k.Path)
}

func newStringArrayKey(path string, description string, defaultValue []string) *StringArrayKey {
	k := newKeyDefinition(path, description, defaultValue)
	v := configflags.StringArrayFlag{}
	flag.Var(&v, path, description)
	return &StringArrayKey{k, v}
}
