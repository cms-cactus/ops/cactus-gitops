package config

import (
	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("package", "config")
