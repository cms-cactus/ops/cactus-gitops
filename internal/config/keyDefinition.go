package config

import "github.com/spf13/viper"

// KeyDefinition describes a config key, to be used as reference in other parts of the application
type KeyDefinition struct {
	// Type types.BasicKind
	// Path denotes where to find this config key in a config file ("log:\n  level: value") or CLI (--log.level=value)
	Path         string
	Description  string
	DefaultValue interface{}
}

// Getter describes the way to retrieve a config key value
type Getter interface {
	Get() interface{}
}

func newKeyDefinition(path string, description string, defaultValue interface{}) *KeyDefinition {
	viper.SetDefault(path, defaultValue)
	return &KeyDefinition{path, description, defaultValue}
}
