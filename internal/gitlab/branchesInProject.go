package gitlab

import (
	"cactus-gitops/internal/config"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

// Commit houses some of the GitLab API
// fields describing a gitlab branch
type Commit struct {
	SHA            string    `json:"id"`
	ShortSHA       string    `json:"short_id"`
	CreatedAt      time.Time `json:"created_at"` // ex: "2020-05-22T18:36:51.660+02:00"
	Title          string
	Message        string
	Author         string `json:"author_name"`
	AuthorEmail    string `json:"author_email"`
	Committer      string `json:"committer_name"`
	CommitterEmail string `json:"committer_email"`
	URL            string `json:"web_url"`
}

// Branch houses some of the GitLab API
// fields describing a gitlab branch
type Branch struct {
	Name            string
	Commit          Commit
	Merged          bool
	Protected       bool
	DevsCanPush     bool   `json:"developers_can_push"`
	DevsCanMerge    bool   `json:"developers_can_merge"`
	IsDefaultBranch bool   `json:"default"`
	URL             string `json:"web_url"`
}

// BranchesInProject returns all projects directly under the given GitLab group ID
func BranchesInProject(id config.ProjectID) ([]Branch, error) {
	_id := fmt.Sprintf("%d", id)
	baseURL := "https://gitlab.cern.ch/api/v4/projects/" + _id + "/repository/branches?per_page=100&page="
	page := "1"
	client := &http.Client{}

	branches := make([]Branch, 0)
	for page != "" {
		u := baseURL + page
		log.Println("getting", u)

		req, err := http.NewRequest("GET", u, nil)
		if err != nil {
			return nil, err
		}
		req.Header.Set("PRIVATE-TOKEN", os.Getenv("GITLAB_TOKEN"))

		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		if resp.StatusCode != 200 {
			return nil, errors.New("http request failed (get projects in group " + _id + " returned " + resp.Status + ")")
		}

		page = resp.Header.Get("X-Next-Page")

		defer resp.Body.Close()
		bytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		branchesPage := make([]Branch, 0)
		err = json.Unmarshal(bytes, &branchesPage)
		if err != nil {
			return nil, err
		}
		branches = append(branches, branchesPage...)
	}

	return branches, nil
}
