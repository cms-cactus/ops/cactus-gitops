package gitlab

import (
	"cactus-gitops/internal/config"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

// Project houses some of the GitLab API
// fields describing a gitlab project
type Project struct {
	ID                config.ProjectID
	Description       string
	Name              string
	NameWithNamespace string `json:"name_with_namespace"`
	Path              string
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
	CloneURL          string `json:"http_url_to_repo"`
	WebURL            string `json:"web_url"`
}

// GetProject fetches a project from gitlab by ID
func GetProject(id config.ProjectID) (*Project, error) {
	_id := fmt.Sprintf("%d", id)
	url := "https://gitlab.cern.ch/api/v4/projects/" + _id
	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("PRIVATE-TOKEN", os.Getenv("GITLAB_TOKEN"))

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("http request failed (get project " + _id + " returned " + resp.Status + ")")
	}
	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	project := Project{}
	err = json.Unmarshal(bytes, &project)
	if err != nil {
		return nil, err
	}
	return &project, nil
}
