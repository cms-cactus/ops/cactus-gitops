package gitlab

import (
	"cactus-gitops/internal/config"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/h2non/gock.v1"
)

func TestRecursionWorks(t *testing.T) {
	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/subgroups").
		Times(1).
		Reply(200).
		BodyString(`[{"id": 200}, {"id": 201}]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/projects").
		Times(1).
		Reply(200).
		BodyString(`[]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/200/projects").
		Times(1).
		Reply(200).
		BodyString(`[{"id": 1}, {"id": 2}]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/200/subgroups").
		Times(1).
		Reply(200).
		BodyString(`[{"id": 202}, {"id": 203}]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/201/projects").
		Times(1).
		Reply(200).
		BodyString(`[{"id": 3}, {"id": 4}]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/202/projects").
		Times(1).
		Reply(200).
		BodyString(`[{"id": 5}, {"id": 6}]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/203/projects").
		Times(1).
		Reply(200).
		BodyString(`[{"id": 7}, {"id": 8}]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/201/subgroups").
		Times(1).
		Reply(200).
		BodyString(`[]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/202/subgroups").
		Times(1).
		Reply(200).
		BodyString(`[]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/203/subgroups").
		Times(1).
		Reply(200).
		BodyString(`[]`)

	projects, err := ProjectsInGroupRecursive(config.GroupID(0))
	require.Nil(t, err)
	require.NotNil(t, projects)
	require.Len(t, projects, 8)
}

func TestRecursiveHandlesErrors(t *testing.T) {
	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/projects").
		Times(1).
		Reply(500).
		BodyString(`[]`)
	_, err := ProjectsInGroupRecursive(config.GroupID(0))
	require.NotNil(t, err)
	gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/projects").
		Times(1).
		Reply(200).
		BodyString(`[]`)
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/subgroups").
		Times(1).
		Reply(500).
		BodyString(`[{"id": 200}, {"id": 201}]`)
	_, err = ProjectsInGroupRecursive(config.GroupID(0))
	require.NotNil(t, err)
}
