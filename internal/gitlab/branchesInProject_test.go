package gitlab

import (
	"cactus-gitops/internal/config"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/h2non/gock.v1"
)

func TestBranchesInbranchesendsCredentials(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/projects/0/repository/branches").
		MatchHeader("PRIVATE-TOKEN", "^fake_gitlab_token$").
		Times(1).
		Reply(200).
		BodyString("[]")

	_, err := BranchesInProject(config.ProjectID(0))
	require.Nil(t, err)
}

func TestBranchesInProjectFailsOnNon200Code(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/projects/0/repository/branches").
		Times(1).
		Reply(400).
		BodyString("[]")

	_, err := BranchesInProject(config.ProjectID(0))
	require.NotNil(t, err)
}

func TestBranchesInProjectFailsOnShittyPayload(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/projects/0/repository/branches").
		Times(1).
		Reply(200).
		BodyString("shitty")

	_, err := BranchesInProject(config.ProjectID(0))
	require.NotNil(t, err)
}

func TestBranchesInProjectFailsOnEmptyPayload(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/projects/0/repository/branches").
		Times(1).
		Reply(200)

	_, err := BranchesInProject(config.ProjectID(0))
	require.NotNil(t, err)
}

func TestBranchesInProjectReadsAllFields(t *testing.T) {
	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/projects/0/repository/branches").
		MatchHeader("PRIVATE-TOKEN", "^fake_gitlab_token$").
		Times(1).
		Reply(200).
		BodyString(`[{
			"name": "release",
			"commit": {
				"id": "dc74dd39058c9ab0751de9c052295a8d7bb20e00",
				"short_id": "dc74dd39",
				"parent_ids": null,
				"title": "Merge branch 'master' into 'release'",
				"message": "Merge branch 'master' into 'release'",
				"author_name": "Glenn Dirkx",
				"author_email": "glenn.dirkx@cern.ch",
				"committer_name": "Glenn Dirkx",
				"committer_email": "glenn.dirkx@cern.ch",
				"web_url": "https://gitlab.cern.ch/cms-cactus/web/L1CE/-/commit/dc74dd39058c9ab0751de9c052295a8d7bb20e00"
			},
			"merged": false,
			"protected": true,
			"developers_can_push": false,
			"developers_can_merge": false,
			"can_push": true,
			"default": false,
			"web_url": "https://gitlab.cern.ch/cms-cactus/web/L1CE/-/tree/release"
		},
		{
			"name": "test-editor-in-column",
			"commit": {
				"id": "8e40f823063eba620a43b51e6825c626d354cbef",
				"short_id": "8e40f823",
				"parent_ids": null,
				"title": "Editor introduced in-column, has bugs",
				"message": "Editor introduced in-column, has bugs",
				"author_name": "Simone Bologna",
				"author_email": "simone.bologna@cern.ch",
				"committer_name": "Simone Bologna",
				"committer_email": "simone.bologna@cern.ch",
				"web_url": "https://gitlab.cern.ch/cms-cactus/web/L1CE/-/commit/8e40f823063eba620a43b51e6825c626d354cbef"
			},
			"merged": false,
			"protected": false,
			"developers_can_push": false,
			"developers_can_merge": false,
			"can_push": true,
			"default": false,
			"web_url": "https://gitlab.cern.ch/cms-cactus/web/L1CE/-/tree/test-editor-in-column"
		}]`)

	branches, err := BranchesInProject(config.ProjectID(0))
	require.Nil(t, err)
	require.NotNil(t, branches)
	require.Len(t, branches, 2)

	// testDate := time.Date(2018, time.November, 14, 12, 35, 38, 000000000, time.UTC)

	require.Equal(t, branches[0], Branch{
		Name: "release",
		Commit: Commit{
			SHA:      "dc74dd39058c9ab0751de9c052295a8d7bb20e00",
			ShortSHA: "dc74dd39",
			// CreatedAt:      testDate,
			Title:          "Merge branch 'master' into 'release'",
			Message:        "Merge branch 'master' into 'release'",
			Author:         "Glenn Dirkx",
			AuthorEmail:    "glenn.dirkx@cern.ch",
			Committer:      "Glenn Dirkx",
			CommitterEmail: "glenn.dirkx@cern.ch",
			URL:            "https://gitlab.cern.ch/cms-cactus/web/L1CE/-/commit/dc74dd39058c9ab0751de9c052295a8d7bb20e00",
		},
		Merged:          false,
		Protected:       true,
		DevsCanPush:     false,
		DevsCanMerge:    false,
		IsDefaultBranch: false,
		URL:             "https://gitlab.cern.ch/cms-cactus/web/L1CE/-/tree/release",
	})

	require.Equal(t, branches[1], Branch{
		Name: "test-editor-in-column",
		Commit: Commit{
			SHA:      "8e40f823063eba620a43b51e6825c626d354cbef",
			ShortSHA: "8e40f823",
			// CreatedAt:      testDate,
			Title:          "Editor introduced in-column, has bugs",
			Message:        "Editor introduced in-column, has bugs",
			Author:         "Simone Bologna",
			AuthorEmail:    "simone.bologna@cern.ch",
			Committer:      "Simone Bologna",
			CommitterEmail: "simone.bologna@cern.ch",
			URL:            "https://gitlab.cern.ch/cms-cactus/web/L1CE/-/commit/8e40f823063eba620a43b51e6825c626d354cbef",
		},
		Merged:          false,
		Protected:       false,
		DevsCanPush:     false,
		DevsCanMerge:    false,
		IsDefaultBranch: false,
		URL:             "https://gitlab.cern.ch/cms-cactus/web/L1CE/-/tree/test-editor-in-column",
	})
}

func TestBranchesInProjectPaginatesCorrectly(t *testing.T) {
	defer gock.Off()

	pages := 5
	for i := 1; i <= pages; i++ {
		id := fmt.Sprintf("%d", i)
		a := gock.New("https://gitlab.cern.ch").
			Get("/api/v4/projects/0/repository/branches").
			MatchParam("page", id).
			Times(1).
			Reply(200).
			BodyString("[{\"name\": \"" + id + "\"}]")

		if i < pages {
			a.AddHeader("X-Next-Page", fmt.Sprintf("%d", i+1))
		}
	}

	branches, err := BranchesInProject(config.ProjectID(0))
	require.Nil(t, err)
	require.NotNil(t, branches)
	require.Len(t, branches, 5)
	require.Equal(t, branches[0].Name, "1")
	require.Equal(t, branches[1].Name, "2")
	require.Equal(t, branches[2].Name, "3")
	require.Equal(t, branches[3].Name, "4")
	require.Equal(t, branches[4].Name, "5")
}
