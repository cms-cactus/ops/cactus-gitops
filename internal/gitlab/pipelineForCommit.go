package gitlab

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"time"

	"cactus-gitops/internal/config"
)

// Pipeline holds the data returned by the GitLab pipeline API
type Pipeline struct {
	ID        int `json:"id"`
	SHA       string
	Ref       string
	Status    string
	CreatedAt time.Time `json:"created_at"` // ex: "2020-05-22T18:36:51.660+02:00"
	UpdatedAt time.Time `json:"updated_at"`
	URL       string    `json:"web_url"`
}

// PipelineForCommit fetches the latest pipeline data for a given commit hash, if any
func PipelineForCommit(id config.ProjectID, sha string) (*Pipeline, error) {
	_id := fmt.Sprintf("%d", id)
	match, err := regexp.Match(`\b[0-9a-f]{5,40}\b`, []byte(sha))
	if err != nil {
		return nil, err
	}
	if !match {
		return nil, errors.New("no valid regex supplied: " + sha)
	}
	URL := "https://gitlab.cern.ch/api/v4/projects/" + _id + "/pipelines?order_by=id&per_page=1&sha=" + sha
	client := &http.Client{}

	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("PRIVATE-TOKEN", os.Getenv("GITLAB_TOKEN"))

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("http request failed (get pipelines in project " + _id + " for sha " + sha + " returned " + resp.Status + ")")
	}
	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	pipelines := make([]Pipeline, 0)
	err = json.Unmarshal(bytes, &pipelines)
	if err != nil {
		return nil, err
	}

	if len(pipelines) == 0 {
		return nil, nil
	}

	return &pipelines[0], nil
}
