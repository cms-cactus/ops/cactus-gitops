package gitlab

import "cactus-gitops/internal/config"

// ProjectsInGroupRecursive returns all gitlab projects in a group and any subgroups under it
func ProjectsInGroupRecursive(id config.GroupID) ([]Project, error) {
	projects := make([]Project, 0)
	todoGroups := []config.GroupID{id}

	for len(todoGroups) != 0 {
		groupID := todoGroups[0]
		todoGroups = todoGroups[1:]

		p, err := ProjectsInGroup(groupID)
		if err != nil {
			return nil, err
		}
		g, err := GroupsInGroup(groupID)
		if err != nil {
			return nil, err
		}

		projects = append(projects, p...)
		todoGroups = append(todoGroups, g...)
	}

	return projects, nil
}
