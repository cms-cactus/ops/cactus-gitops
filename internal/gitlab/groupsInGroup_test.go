package gitlab

import (
	"cactus-gitops/internal/config"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/h2non/gock.v1"
)

func TestGroupsInGroupSendsCredentials(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/subgroups").
		MatchHeader("PRIVATE-TOKEN", "^fake_gitlab_token$").
		Times(1).
		Reply(200).
		BodyString("[]")

	_, err := GroupsInGroup(config.GroupID(0))
	require.Nil(t, err)
}

func TestGroupsInGroupFailsOnNon200Code(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/subgroups").
		Times(1).
		Reply(400).
		BodyString("[]")

	_, err := GroupsInGroup(config.GroupID(0))
	require.NotNil(t, err)
}

func TestGroupsInGroupFailsOnShittyPayload(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/subgroups").
		Times(1).
		Reply(200).
		BodyString("shitty")

	_, err := GroupsInGroup(config.GroupID(0))
	require.NotNil(t, err)
}

func TestGroupsInGroupFailsOnEmptyPayload(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/subgroups").
		Times(1).
		Reply(200)

	_, err := GroupsInGroup(config.GroupID(0))
	require.NotNil(t, err)
}

func TestGroupsInGroupReadsAllFields(t *testing.T) {
	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/subgroups").
		MatchHeader("PRIVATE-TOKEN", "^fake_gitlab_token$").
		Times(1).
		Reply(200).
		BodyString(`[{"id": 1}, {"id": 20}]`)

	groups, err := GroupsInGroup(config.GroupID(0))
	require.Nil(t, err)
	require.NotNil(t, groups)
	require.Len(t, groups, 2)

	require.Equal(t, groups[0], config.GroupID(1))

	require.Equal(t, groups[1], config.GroupID(20))
}

func TestGroupsInGroupPaginatesCorrectly(t *testing.T) {
	defer gock.Off()

	pages := 5
	for i := 1; i <= pages; i++ {
		id := fmt.Sprintf("%d", i)
		a := gock.New("https://gitlab.cern.ch").
			Get("/api/v4/groups/0/subgroups").
			MatchParam("page", id).
			Times(1).
			Reply(200).
			BodyString("[{\"id\":" + id + "}]")

		if i < pages {
			a.AddHeader("X-Next-Page", fmt.Sprintf("%d", i+1))
		}
	}

	groups, err := GroupsInGroup(0)
	require.Nil(t, err)
	require.NotNil(t, groups)
	require.Len(t, groups, 5)
	require.Equal(t, groups[0], config.GroupID(1))
	require.Equal(t, groups[1], config.GroupID(2))
	require.Equal(t, groups[2], config.GroupID(3))
	require.Equal(t, groups[3], config.GroupID(4))
	require.Equal(t, groups[4], config.GroupID(5))
}
