package gitlab

import (
	"cactus-gitops/internal/config"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// ProjectsInGroup returns all projects directly under the given GitLab group ID
func ProjectsInGroup(id config.GroupID) ([]Project, error) {
	_id := fmt.Sprintf("%d", id)
	baseURL := "https://gitlab.cern.ch/api/v4/groups/" + _id + "/projects?per_page=100&page="
	page := "1"
	client := &http.Client{}

	projects := make([]Project, 0)
	for page != "" {
		u := baseURL + page
		log.Println("getting", u)

		req, err := http.NewRequest("GET", u, nil)
		if err != nil {
			return nil, err
		}
		req.Header.Set("PRIVATE-TOKEN", os.Getenv("GITLAB_TOKEN"))

		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		if resp.StatusCode != 200 {
			return nil, errors.New("http request failed (get projects in group " + _id + " returned " + resp.Status + ")")
		}

		page = resp.Header.Get("X-Next-Page")

		defer resp.Body.Close()
		bytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		projectsPage := make([]Project, 0)
		err = json.Unmarshal(bytes, &projectsPage)
		if err != nil {
			return nil, err
		}
		projects = append(projects, projectsPage...)
	}

	return projects, nil
}
