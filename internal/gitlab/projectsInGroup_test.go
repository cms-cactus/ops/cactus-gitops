package gitlab

import (
	"cactus-gitops/internal/config"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/h2non/gock.v1"
)

func TestProjectsInGroupSendsCredentials(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/projects").
		MatchHeader("PRIVATE-TOKEN", "^fake_gitlab_token$").
		Times(1).
		Reply(200).
		BodyString("[]")

	_, err := ProjectsInGroup(config.GroupID(0))
	require.Nil(t, err)
}

func TestProjectsInGroupFailsOnNon200Code(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/projects").
		Times(1).
		Reply(400).
		BodyString("[]")

	_, err := ProjectsInGroup(config.GroupID(0))
	require.NotNil(t, err)
}

func TestProjectsInGroupFailsOnShittyPayload(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/projects").
		Times(1).
		Reply(200).
		BodyString("shitty")

	_, err := ProjectsInGroup(config.GroupID(0))
	require.NotNil(t, err)
}

func TestProjectsInGroupFailsOnEmptyPayload(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/projects").
		Times(1).
		Reply(200)

	_, err := ProjectsInGroup(config.GroupID(0))
	require.NotNil(t, err)
}

func TestProjectsInGroupReadsAllFields(t *testing.T) {
	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("/api/v4/groups/0/projects").
		MatchHeader("PRIVATE-TOKEN", "^fake_gitlab_token$").
		Times(1).
		Reply(200).
		BodyString(`[{
			"id": 1,
			"description": "description of project 1",
			"name": "project 1",
			"name_with_namespace": "ns/project1",
			"path": "/project1",
			"path_with_namespace": "ns/project1",
			"default_branch": "master",
			"http_url_to_repo": "http://project1.git",
			"web_url": "http://project1"
		}, {
			"id": 20,
			"description": "description of project 20",
			"name": "project 20",
			"name_with_namespace": "ns/project20",
			"path": "/project20",
			"path_with_namespace": "ns/project20",
			"default_branch": "master",
			"http_url_to_repo": "http://project20.git",
			"web_url": "http://project20"
		}]`)

	projects, err := ProjectsInGroup(config.GroupID(0))
	require.Nil(t, err)
	require.NotNil(t, projects)
	require.Len(t, projects, 2)

	require.Equal(t, projects[0].ID, config.ProjectID(1))
	require.Equal(t, projects[0].Description, "description of project 1")
	require.Equal(t, projects[0].Name, "project 1")
	require.Equal(t, projects[0].NameWithNamespace, "ns/project1")
	require.Equal(t, projects[0].Path, "/project1")
	require.Equal(t, projects[0].PathWithNamespace, "ns/project1")
	require.Equal(t, projects[0].DefaultBranch, "master")
	require.Equal(t, projects[0].CloneURL, "http://project1.git")
	require.Equal(t, projects[0].WebURL, "http://project1")

	require.Equal(t, projects[1].ID, config.ProjectID(20))
	require.Equal(t, projects[1].Description, "description of project 20")
	require.Equal(t, projects[1].Name, "project 20")
	require.Equal(t, projects[1].NameWithNamespace, "ns/project20")
	require.Equal(t, projects[1].Path, "/project20")
	require.Equal(t, projects[1].PathWithNamespace, "ns/project20")
	require.Equal(t, projects[1].DefaultBranch, "master")
	require.Equal(t, projects[1].CloneURL, "http://project20.git")
	require.Equal(t, projects[1].WebURL, "http://project20")
}

func TestProjectsInGroupPaginatesCorrectly(t *testing.T) {
	defer gock.Off()

	pages := 5
	for i := 1; i <= pages; i++ {
		id := fmt.Sprintf("%d", i)
		a := gock.New("https://gitlab.cern.ch").
			Get("/api/v4/groups/0/projects").
			MatchParam("page", id).
			Times(1).
			Reply(200).
			BodyString("[{\"id\":" + id + "}]")

		if i < pages {
			a.AddHeader("X-Next-Page", fmt.Sprintf("%d", i+1))
		}
	}

	projects, err := ProjectsInGroup(config.GroupID(0))
	require.Nil(t, err)
	require.NotNil(t, projects)
	require.Len(t, projects, 5)
	require.Equal(t, projects[0].ID, config.ProjectID(1))
	require.Equal(t, projects[1].ID, config.ProjectID(2))
	require.Equal(t, projects[2].ID, config.ProjectID(3))
	require.Equal(t, projects[3].ID, config.ProjectID(4))
	require.Equal(t, projects[4].ID, config.ProjectID(5))
}
