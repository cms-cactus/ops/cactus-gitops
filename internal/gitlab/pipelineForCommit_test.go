package gitlab

import (
	"cactus-gitops/internal/config"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gopkg.in/h2non/gock.v1"
)

const dummySHA = "aaaaaaa"

func TestPipelineForCommitSendsCredentials(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("api/v4/projects/0/pipelines").
		MatchHeader("PRIVATE-TOKEN", "^fake_gitlab_token$").
		Times(1).
		Reply(200).
		BodyString("[]")

	_, err := PipelineForCommit(config.ProjectID(0), dummySHA)
	require.Nil(t, err)
}

func TestPipelineForCommitFailsOnNon200Code(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("api/v4/projects/0/pipelines").
		Times(1).
		Reply(400).
		BodyString("[]")

	_, err := PipelineForCommit(config.ProjectID(0), dummySHA)
	require.NotNil(t, err)
}

func TestPipelineForCommitFailsOnShittyPayload(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("api/v4/projects/0/pipelines").
		Times(1).
		Reply(200).
		BodyString("shitty")

	_, err := PipelineForCommit(config.ProjectID(0), dummySHA)
	require.NotNil(t, err)
}

func TestPipelineForCommitFailsOnEmptyPayload(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "fake_gitlab_token")

	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("api/v4/projects/0/pipelines").
		Times(1).
		Reply(200)

	_, err := PipelineForCommit(config.ProjectID(0), dummySHA)
	require.NotNil(t, err)
}

func TestPipelineForCommitReadsAllFields(t *testing.T) {
	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("api/v4/projects/0/pipelines").
		MatchHeader("PRIVATE-TOKEN", "^fake_gitlab_token$").
		Times(1).
		Reply(200).
		BodyString(`[{
			"id": 1654367,
			"sha": "1ca64dc6fa2b16fb6e499ba29a06d2ce8d044f2e",
			"ref": "master",
			"status": "manual",
			"created_at": "2020-05-22T18:36:51.660+02:00",
			"updated_at": "2020-05-22T18:43:32.167+02:00",
			"web_url": "https://gitlab.cern.ch/cms-cactus/experimental/auto-devops-rpm-makefile-demo/pipelines/1654367"
		},
		{
			"id": 1654345,
			"sha": "1ca64dc6fa2b16fb6e499ba29a06d2ce8d044f2e",
			"ref": "master",
			"status": "failed",
			"created_at": "2020-05-22T18:29:02.496+02:00",
			"updated_at": "2020-05-22T18:34:29.866+02:00",
			"web_url": "https://gitlab.cern.ch/cms-cactus/experimental/auto-devops-rpm-makefile-demo/pipelines/1654345"
		}]`)

	pipeline, err := PipelineForCommit(config.ProjectID(0), dummySHA)
	require.Nil(t, err)
	require.NotNil(t, pipeline)

	require.Equal(t, pipeline.ID, 1654367)
	require.Equal(t, pipeline.SHA, "1ca64dc6fa2b16fb6e499ba29a06d2ce8d044f2e")
	require.Equal(t, pipeline.Ref, "master")
	require.Equal(t, pipeline.Status, "manual")
	require.NotNil(t, pipeline.CreatedAt)
	require.NotNil(t, pipeline.UpdatedAt)
	require.Equal(t, pipeline.URL, "https://gitlab.cern.ch/cms-cactus/experimental/auto-devops-rpm-makefile-demo/pipelines/1654367")
}

func TestPipelineForCommitParsesDatesCorrectly(t *testing.T) {
	defer gock.Off()
	gock.New("https://gitlab.cern.ch").
		Get("api/v4/projects/0/pipelines").
		MatchHeader("PRIVATE-TOKEN", "^fake_gitlab_token$").
		Times(1).
		Reply(200).
		BodyString(`[{
			"created_at": "2020-05-22T18:36:51.660+02:00"
		}]`)

	pipeline, err := PipelineForCommit(config.ProjectID(0), dummySHA)
	require.Nil(t, err)
	require.NotNil(t, pipeline)

	createdAt := time.Date(2020, time.May, 22, 18, 36, 51, 660000000, time.FixedZone("UTC+2", 2*60*60))

	require.Equal(t, pipeline.CreatedAt.UTC(), createdAt.UTC())
	require.True(t, pipeline.CreatedAt.Equal(createdAt)) // preferred method, but this doesn't provide useful diffs on fail

}

func TestShittySHA(t *testing.T) {
	shittySHAs := []string{"", "a", "aaa", "abc de202", "1ca64dc6fa2b16fb6e499ba29a06d2ce8d044f2ef", "1ca64dc6fa2g16fb6e499ba29a06d2ce8d044f2e"}
	for _, sha := range shittySHAs {
		pipeline, err := PipelineForCommit(config.ProjectID(0), sha)
		require.NotNil(t, err)
		require.Nil(t, pipeline)
	}
}

func TestValidSHA(t *testing.T) {
	validSHAs := []string{"1ca64dc6fa2b16fb6e499ba29a06d2ce8d044f2e", "1ca64dc6"}
	for _, sha := range validSHAs {
		pipeline, err := PipelineForCommit(config.ProjectID(0), sha)
		require.NotNil(t, err)
		require.Nil(t, pipeline)
	}
}
