package gitlab

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"cactus-gitops/internal/config"
)

type groupResponse struct {
	ID config.GroupID
}

// GroupsInGroup returns all direct subgroups of the given gitlab group ID
func GroupsInGroup(id config.GroupID) ([]config.GroupID, error) {
	_id := fmt.Sprintf("%d", id)
	baseURL := "https://gitlab.cern.ch/api/v4/groups/" + _id + "/subgroups?per_page=100&page="
	page := "1"
	client := &http.Client{}

	groups := make([]config.GroupID, 0)
	for page != "" {
		u := baseURL + page
		log.Println("getting", u)

		req, err := http.NewRequest("GET", u, nil)
		if err != nil {
			return nil, err
		}
		req.Header.Set("PRIVATE-TOKEN", os.Getenv("GITLAB_TOKEN"))

		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		if resp.StatusCode != 200 {
			return nil, errors.New("http request failed (get subgroups in group " + _id + " returned " + resp.Status + ")")
		}

		page = resp.Header.Get("X-Next-Page")

		defer resp.Body.Close()
		bytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		groupsPage := make([]groupResponse, 0)
		err = json.Unmarshal(bytes, &groupsPage)
		if err != nil {
			return nil, err
		}
		for _, g := range groupsPage {
			groups = append(groups, g.ID)
		}
	}

	return groups, nil
}
