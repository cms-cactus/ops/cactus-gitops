package scraper

import (
	"cactus-gitops/internal/config"
	"cactus-gitops/internal/gitlab"
	"strconv"
	"time"
)

func projectInList(id config.ProjectID, list []gitlab.Project) bool {
	for _, project := range list {
		if project.ID == id {
			return true
		}
	}
	return false
}

var noTime = time.Time{}

// GetProjectList gets all gitlab projects specified in the configuration
func GetProjectList() ([]gitlab.Project, error) {
	if cacheAge != noTime {
		refreshRate, err := time.ParseDuration(config.ConfigGitlabRefreshProjectList.Get())
		if err != nil {
			return nil, err
		}
		if !time.Now().After(cacheAge.Add(refreshRate)) {
			log.Info("re-using cached project list")
			return projectCache, nil
		}
	}

	groupsToScrape := config.ConfigGitlabGroups.Get()
	projectsToScrape := config.ConfigGitlabProjects.Get()

	scrapedProjects := []gitlab.Project{}

	for i, groupToScrape := range groupsToScrape {
		if groupToScrape == "" {
			log.WithField("index", i).Error("empty group provided in config")
			continue
		}

		groupID, err := strconv.ParseInt(groupToScrape, 10, 32)
		if err != nil {
			log.WithError(err).WithField("index", i).WithField("group", groupToScrape).Error("skipping invalid group ID")
			continue
		}

		projects, err := gitlab.ProjectsInGroupRecursive(config.GroupID(groupID))
		if err != nil {
			return nil, err
		}
		for _, project := range projects {
			if !projectInList(project.ID, scrapedProjects) {
				scrapedProjects = append(scrapedProjects, project)
			}
		}
	}

	for i, projectToScrape := range projectsToScrape {
		if projectToScrape == "" {
			log.WithField("index", i).Error("empty project provided in config")
			continue
		}
		projectID, err := strconv.ParseInt(projectToScrape, 10, 32)
		if err != nil {
			log.WithError(err).WithField("index", i).WithField("group", projectToScrape).Error("skipping invalid project ID")
			continue
		}
		if !projectInList(config.ProjectID(projectID), scrapedProjects) {
			project, err := gitlab.GetProject(config.ProjectID(projectID))
			if err != nil {
				return nil, err
			}
			scrapedProjects = append(scrapedProjects, *project)
		}

	}

	cacheAge = time.Now()
	projectCache = scrapedProjects
	return projectCache, nil
}
