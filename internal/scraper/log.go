package scraper

import (
	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("package", "scraper")
