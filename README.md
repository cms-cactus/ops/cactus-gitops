# cactus-gitops

This application watches a configured list of gitlab projects or groups,
and deploys them to the cactus-review cluster.

# How will my project be included?
cactus-gitops is configured with a list of projects and groups.
You can view this list [here](TODO)
If your project is in this list, or in a group in this list, it will automatically be included.
Periodic checks exist to check for newly created projects.

# How will code in my project be deployed?
cactus-gitops does not automatically build your project. It only applies Kubernetes manifest files in the following manner:
```bash
$ kubectl apply -Rf <manifest_folder> --prune --all
```
It only applies this to commits that have a passed 'green' GitLab CI pipeline status.

A namespace is created per branch per project, and is agnostic to the branching strategy used in your project.

cactus-gitops will act on your project in the following order
- look for a .gitlab/cactus-review.yml config file
  - look for a `preset` config value (e.g.: `preset: swatch`)  
    in this case, no kubernetes config is needed on your part  
    available presets can be seen [here](TODO)
  - look for a `manifests` config value (e.g.: `manifests: k8s`)  
    this denotes the folder where kubernetes manifests to deploy can be found
- if no config file is found, it defaults to a `manifest` config of `k8s`

## Manifest templates
You can make use of [go templates](https://golang.org/pkg/text/template/) ([example](https://gowebexamples.com/templates/)).

The data available to you for templating is defined [here](TODO)

## Integrating with Gitlab CI
Although cactus-gitops will pick up your code pushes automatically, you can manually trigger a sync with your branch using
an HTTP call:
```
TODO curl command
```

This can be useful if 
- you want to make sure the deployment is updated in a timely manner
- you want to create a gitlab deployment entry in your projects interface.
If this curl command is used, cactus-gitops will exceptionally also allow your commit pipeline to still be in the 'running' state.

An template job is provided for you and can be used in the following way
```
TODO
```

# Security
cactus-gitops will only perform a Kubernetes deployment if any of the following criteria are met
- the commit author is listed in the CODEOWNERS file on the master branch
- the commit author has previous commits on the master branch
- the commit does not 