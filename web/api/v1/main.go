package apiv1

import (
	"fmt"
	"net/http"
	"sort"
	"strings"
	"time"

	"cactus-gitops/web/api/v1/util/errors"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
)

var v1router = makeRouter()

// Register adds the v1router to the given router
func Register(router *chi.Mux) {
	v1router.Get("/", listAllEndpoints)
	router.Mount("/api/v1", v1router)
}

func makeRouter() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(panicHandler)
	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped
	r.Use(middleware.Timeout(5 * time.Minute))

	r.NotFound(statusReturner(http.StatusNotFound))
	r.MethodNotAllowed(statusReturner(http.StatusMethodNotAllowed))
	return r
}

func statusReturner(code int) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		errors.Error(code).Send(w, r)
	}
}

func listAllEndpoints(w http.ResponseWriter, r *http.Request) {
	endpointsMap := make(map[string][]string)

	walker := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		entry := endpointsMap[route]
		if entry == nil {
			entry = []string{method}
		} else {
			entry = append(entry, method)
		}
		endpointsMap[route] = entry
		return nil
	}
	err := chi.Walk(v1router, walker)
	if err != nil {
		errors.Error(500).Send(w, r)
		return
	}

	endpoints := make([]string, len(endpointsMap))

	i := 0
	for route, methods := range endpointsMap {
		sort.Strings(methods)
		endpoints[i] = fmt.Sprintf("(%s) %s", strings.Join(methods, ","), route)
		i++
	}

	sort.Strings(endpoints)

	render.JSON(w, r, endpoints)
}
