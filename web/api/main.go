package api

import (
	"cactus-gitops/web/api/router"
	v1 "cactus-gitops/web/api/v1"
	"net"
	"net/http"

	"github.com/go-chi/chi"
)

// StartAndListen starts the API and opens a socket on the specified port
func StartAndListen(port string) error {

	r := chi.NewRouter()

	v1.Register(r)
	router.RegisterWebUI(r)

	if port == "" {
		port = "0"
	}
	listener, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return err
	}
	address := "http://" + listener.Addr().String()
	log.WithField("address", address).Info("listening")
	return http.Serve(listener, r)
}
