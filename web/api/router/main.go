package router

import (
	"cactus-gitops/web/ui"
	"net/http"

	"github.com/go-chi/chi"
)

// RegisterWebUI adds a default handler to the given router that serves the web ui
func RegisterWebUI(router *chi.Mux) {
	h := http.FileServer(ui.Assets)
	router.Get("/*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r)
	}))
}
