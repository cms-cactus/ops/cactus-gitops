module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/essential",
    "plugin:prettier/recommended",
    "@vue/typescript/recommended",
    "@vue/prettier",
    "@vue/prettier/@typescript-eslint"
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    "no-console": [
      process.env.NODE_ENV === "production" ? "error" : "off",
      { allow: ["warn", "error"] }
    ],
    "no-debugger": [process.env.NODE_ENV === "production" ? "error" : "warn"],
    "@typescript-eslint/no-var-requires": 0, // interferes with js files (webpack config, e2e tests)
    "@typescript-eslint/no-use-before-define": [
      "error",
      { functions: false, classes: true, typedefs: false }
    ],
    "@typescript-eslint/explicit-module-boundary-types": 0
  },
  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)"
      ],
      env: {
        mocha: true
      }
    }
  ]
};
