//go:generate go run production.go
// +build ignore

package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/shurcooL/vfsgen"
)

func main() {
	err := vfsgen.Generate(http.Dir("../build"), vfsgen.Options{
		PackageName:  "ui",
		BuildTags:    "production",
		VariableName: "Assets",
		Filename:     "../production.go",
	})
	if err != nil {
		fmt.Fprint(os.Stderr, "error while generating static assets:", err)
		os.Exit(1)
	}
}
