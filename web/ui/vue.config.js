const webpack = require("webpack");

module.exports = {
  outputDir: "build",
  configureWebpack: {
    resolve: {
      alias: {
        // override to use jquery/src instead of jquery/dist
        // gives better tree shaking
        jquery: "jquery/src/jquery"
      }
    },
    plugins: [
      // when you use javascript modules that rely on jquery,
      // they will expect '$' or 'jQuery' to be available globally
      // with this, webpack will detect these variables being used
      // and import query for these modules
      // This will also make $ and jQuery available globally during runtime
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
      })
    ],
    performance: {
      // we serve a locally hosted app, so we can go pretty far
      // with asset file sizes, but fail a build when it gets ridiculous
      hints: "error",
      // individual asset size
      maxAssetSize: 5242880, // 5MiB
      // total size
      maxEntrypointSize: 31457280 // 30MiB
      // assetFilter: function(assetFilename) {
      //   // Function predicate that provides asset filenames
      //   return assetFilename.endsWith(".css") || assetFilename.endsWith(".js");
      // }
    },
    optimization: {
      splitChunks: {
        chunks: "all"
      }
    },
    devtool: "source-map"
  },
  chainWebpack: config => {
    config.module
      .rule("eslint")
      .use("eslint-loader")
      .options({
        fix: true
      });
  },
  transpileDependencies: ["vuetify"]
};
