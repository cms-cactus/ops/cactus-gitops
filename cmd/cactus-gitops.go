package main

import (
	"cactus-gitops/internal/config"
	"cactus-gitops/internal/gitlab"
	"cactus-gitops/internal/scraper"
	"cactus-gitops/web/api"
	"flag"
	"fmt"
	"time"
)

func main() {
	log.Info("starting")

	initConfig()

	refreshRate, err := time.ParseDuration(config.ConfigGitlabRefreshProjectContent.Get())
	if err != nil {
		log.WithError(err).Fatal("cannot parse project content refresh rate")
	}

	lastProjectlist, err := firstProjectListScrape()

	go func() {
		for {
			newProjectList, err := scraper.GetProjectList()
			if err == nil {
				lastProjectlist = newProjectList
			} else {
				newProjectList = lastProjectlist
			}
			time.Sleep(refreshRate)
		}
	}()

	port := fmt.Sprint(config.ConfigAPIPort.Get())
	err = api.StartAndListen(port)
	if err != nil {
		log.WithError(err).Fatal("cannot start api")
	}
}

func initConfig() {
	configFile := flag.String("config.file", "", "the config file to use")
	flag.Parse()
	err := config.ReadConfigAndReloadOnSighup(*configFile)
	if err != nil {
		log.WithError(err).Fatal("could not read config")
	}
	log.WithField("loglevel", config.ConfigLoglevel.Get()).Info("config path")
}

func firstProjectListScrape() ([]gitlab.Project, error) {
	projects, err := scraper.GetProjectList()
	if err != nil {
		log.WithError(err).Fatal("cannot get project list from GitLab")
	}
	{
		projectPaths := make([]string, len(projects))
		for i, project := range projects {
			projectPaths[i] = project.PathWithNamespace
		}
		log.WithField("projects", projectPaths).Info("first fetch of projects")
	}
	return projects, nil
}
